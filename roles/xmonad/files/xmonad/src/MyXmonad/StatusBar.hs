module MyXmonad.StatusBar
  ( addStatusBar
  )
where

import MyXmonad.Layout (LayoutName (..), getLayoutName)
import MyXmonad.Shared.AppData (AppData)
import MyXmonad.Shared.AppData qualified as AppData
import MyXmonad.Shared.Theme qualified as Theme
import MyXmonad.Shared.Theme.Color qualified as Color
import MyXmonad.Utils (numberOfWindowsOnWorkspace)
import XMonad (X)
import XMonad qualified
import XMonad.Core (WorkspaceId)
import XMonad.Hooks.StatusBar qualified as StatusBar
import XMonad.Hooks.StatusBar.PP (PP, xmobarAction)
import XMonad.Hooks.StatusBar.PP qualified as PP
import XMonad.Hooks.StatusBar.WorkspaceScreen (combineWithScreenNumber)
import XMonad.Util.WorkspaceCompare (getSortByTag)


fullScreenWindowCount :: X (Maybe String)
fullScreenWindowCount = do
  currentLayout <- getLayoutName
  numberOfWindows <- numberOfWindowsOnWorkspace
  pure $
    if currentLayout == Full && numberOfWindows > 0
      then Just $ show numberOfWindows
      else Nothing


makeClickable :: Monad m => String -> m String
makeClickable tag =
  pure $ xmobarAction ("xdotool key alt+" <> tag) "1" tag


prefixScreenName :: WorkspaceId -> String -> String
prefixScreenName workspaceId screenNumber =
  let screenName =
        case screenNumber of
          "0" -> " C"
          "2" -> " R"
          "1" -> " L"
          _ -> ""
   in workspaceId <> screenName


makePp :: AppData -> X PP
makePp appData = do
  let
    theme = appData.theme

    color xmobarColor =
      let hexColor = Color.toHashString $ xmobarColor theme.xmobar
       in PP.xmobarColor hexColor ""

    addScreenName
      | length appData.screenInfo > 1 = combineWithScreenNumber prefixScreenName
      | otherwise = pure

  addScreenName $
    PP.def
      { PP.ppCurrent = color (.currentWs) . PP.wrap "[" "]"
      , PP.ppVisible = color (.foreground) . PP.wrap "(" ")"
      , PP.ppUrgent = color (.urgentWs)
      , PP.ppTitle = color (.windowTitle)
      , PP.ppTitleSanitize = PP.xmobarRaw . PP.shorten 40
      , PP.ppExtras = [fullScreenWindowCount]
      , PP.ppRename = makeClickable
      , PP.ppSort = getSortByTag
      }


addStatusBar
  :: XMonad.LayoutClass l XMonad.Window
  => AppData
  -> XMonad.XConfig l
  -> XMonad.XConfig l
addStatusBar appData xconfig =
  let
    pp = makePp appData

    primary = StatusBar.statusBarPropTo "_XMONAD_LOG_1" "xmobar" pp
    secondary = StatusBar.statusBarPropTo "_XMONAD_LOG_2" "xmobar --secondary" pp
    tertiary = StatusBar.statusBarPropTo "_XMONAD_LOG_3" "xmobar --tertiary" pp

    sbConfig
      | length appData.screenInfo > 1 = mconcat [primary, secondary, tertiary]
      | otherwise = primary
   in
    StatusBar.withSB sbConfig xconfig
