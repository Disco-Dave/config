return {
  "arcticicestudio/nord-vim",
  lazy = false,
  priority = 1000,
  init = function()
    local signs = {
      { name = "DiagnosticSignError", text = "" },
      { name = "DiagnosticSignWarn", text = "" },
      { name = "DiagnosticSignHint", text = "" },
      { name = "DiagnosticSignInfo", text = "" },
    }

    for _, sign in pairs(signs) do
      vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.text, numhl = "" })
    end

    vim.opt.cursorline = true                    -- highlight the current line
    vim.g.nord_cursor_line_number_background = 1 -- extend cursorline highlight into the number column
    vim.g.nord_italic = 1                        -- enable italic fonts
    vim.g.nord_bold = 1                          -- enable bold fonts
    vim.g.nord_underline = 1                     -- enable underlined fonts
    vim.g.nord_uniform_diff_background = 1       -- highlight the foreground instead of background when diffing
  end,
  config = function()
    vim.cmd([[colorscheme nord]])
  end,
}
