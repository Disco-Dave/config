module MyXmonad.Shared.AppData
  ( AppData (..)
  , fromConfig
  ) where

import Control.Exception (bracket)
import GHC.Natural (Natural)
import Graphics.X11.Xinerama qualified as Display
import Graphics.X11.Xlib.Display qualified as Display
import Graphics.X11.Xlib.Types (Rectangle)
import MyXmonad.Shared.Config (Config (Config))
import MyXmonad.Shared.Config qualified as Config
import MyXmonad.Shared.FilePaths (FilePaths)
import MyXmonad.Shared.FilePaths qualified as FilePaths
import MyXmonad.Shared.Theme (Theme)


data AppData = AppData
  { filePaths :: FilePaths
  , theme :: Theme
  , startupSound :: Maybe FilePath
  , wallpaper :: Maybe FilePath
  , screenInfo :: [Rectangle]
  , redshiftCoordinates :: Maybe String
  , dmenuFontSize :: Maybe Natural
  }
  deriving (Show, Eq)


fromConfig :: Config -> IO AppData
fromConfig config = do
  let Config{..} = config

  filePaths <- FilePaths.getFilePaths

  screenInfo <-
    bracket
      (Display.openDisplay "")
      Display.closeDisplay
      Display.getScreenInfo

  pure
    AppData
      { ..
      }
