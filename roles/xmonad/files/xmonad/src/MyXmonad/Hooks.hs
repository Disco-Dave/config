module MyXmonad.Hooks
  ( applyHooks
  ) where

import Data.Map.Strict qualified as Map
import Graphics.X11 qualified as X11
import MyXmonad.Shared.AppData (AppData)
import MyXmonad.StatusBar qualified as StatusBar
import XMonad (def)
import XMonad qualified
import XMonad.Actions.FlexibleResize qualified as FlexibleResize
import XMonad.Actions.Navigation2D qualified as Nav2D
import XMonad.Hooks.EwmhDesktops qualified as Ewmh
import XMonad.Hooks.ManageDocks qualified as ManageDocks


resizeByAnyCorner :: XMonad.XConfig l -> XMonad.XConfig l
resizeByAnyCorner oldConfig@XMonad.XConfig{modMask} =
  let newKey = (modMask, X11.button3)
      newAction w = XMonad.focus w *> FlexibleResize.mouseResizeWindow w
   in oldConfig
        { XMonad.mouseBindings = Map.insert newKey newAction . XMonad.mouseBindings oldConfig
        }


applyHooks
  :: XMonad.LayoutClass l XMonad.Window
  => AppData
  -> XMonad.XConfig l
  -> XMonad.XConfig l
applyHooks appData =
  StatusBar.addStatusBar appData
    . Ewmh.ewmhFullscreen
    . Ewmh.ewmh
    . ManageDocks.docks
    . resizeByAnyCorner
    . Nav2D.withNavigation2DConfig def
