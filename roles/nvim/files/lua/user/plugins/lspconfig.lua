return {
  "neovim/nvim-lspconfig",
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    "williamboman/mason-lspconfig.nvim",
  },
  config = function()
    -- https://github.com/neovim/nvim-lspconfig
    -- :help lspconfig
    local lspconfig = require("lspconfig")


    -- https://github.com/hrsh7th/cmp-nvim-lsp
    local cmp_nvim_lsp = require("cmp_nvim_lsp")

    local capabilities = cmp_nvim_lsp.default_capabilities()


    -- :lua =vim.lsp.get_active_clients()[1].server_capabilities
    -- Useful for when I want formatting to come from null-ls.
    local function disable_formatting(client)
      client.server_capabilities.documentFormattingProvider = false
      client.server_capabilities.documentRangeFormattingProvider = false
    end


    -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
    -- :help lspconfig-all


    -- Lua
    lspconfig.lua_ls.setup({
      capabilities = capabilities,
      settings = {
        Lua = {
          diagnostics = {
            globals = { 'vim' },
          },
          telemetry = {
            enable = false,
          },
        },
      },
    })


    -- Haskell
    lspconfig.hls.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
      filetypes = { 'haskell', 'lhaskell', 'cabal' },
      settings = {
        haskell = {
          formattingProvider = "fourmolu",
          plugin = {
            stan = {
              globalOn = false
            }
          }
        },
      },
    })


    -- Elm
    lspconfig.elmls.setup({
      capabilities = capabilities,
    })


    -- Rust
    lspconfig.rust_analyzer.setup({
      capabilities = capabilities,
      settings = {
        ["rust-analyzer"] = {
          check = {
            command = "clippy"
          }
        }
      }
    })


    -- Typescript
    lspconfig.ts_ls.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
    })

    lspconfig.eslint.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
    })

    -- CSS
    lspconfig.cssls.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
    })

    lspconfig.stylelint_lsp.setup({
      capabilities = capabilities,
      filetypes = { "css" },
      on_attach = disable_formatting,
    })


    -- HTML
    lspconfig.html.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
    })


    -- JSON
    lspconfig.jsonls.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
    })


    -- YAML
    lspconfig.yamlls.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
      settings = {
        redhat = {
          telemetry = {
            enabled = false,
          },
        },
        yaml = {
          keyOrdering = false,
          schemas = {
            ["https://raw.githubusercontent.com/ansible/schemas/main/f/ansible.json"] = "deploy.yml",
          }
        },
      },
    })


    -- Docker
    lspconfig.dockerls.setup({
      capabilities = capabilities,
    })

    lspconfig.docker_compose_language_service.setup({
      capabilities = capabilities,
      on_attach = disable_formatting,
    })


    -- Terraform
    lspconfig.terraformls.setup({
      capabilities = capabilities,
    })

    lspconfig.tflint.setup({
      capabilities = capabilities,
    })


    -- Ansible
    lspconfig.ansiblels.setup({
      capabilities = capabilities,
    })

    lspconfig.csharp_ls.setup({
      capabilities = capabilities,
    })
  end
}
