# Personal dotfiles and configuration
Ansible playbook and dotfiles for configuring my personal desktop and laptop.


## Dependencies
- Arch Linux with root access
- Ansible
- Git
