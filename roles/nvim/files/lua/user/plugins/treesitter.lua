return {
  "nvim-treesitter/nvim-treesitter",
  config = function()
    require("nvim-treesitter.configs").setup({
      sync_install = false,
      auto_install = true,
      ensure_installed = {
        "bash",
        "css",
        "dockerfile",
        "elm",
        "haskell",
        "html",
        "javascript",
        "json",
        "lua",
        "rust",
        "sql",
        "terraform",
        "typescript",
        "yaml",
      },

      highlight = {
        enable = true,
        disable = {},
        additional_vim_regex_highlighting = true,
      },
    })
  end,
}
