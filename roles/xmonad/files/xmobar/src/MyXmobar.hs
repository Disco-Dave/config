module MyXmobar (start) where

import MyXmobar.Args (Args (..))
import MyXmonad.Shared.AppData (AppData)
import MyXmonad.Shared.AppData qualified as AppData
import MyXmonad.Shared.Config (Config)
import MyXmonad.Shared.FilePaths qualified as FilePaths
import MyXmonad.Shared.Theme qualified as Theme
import MyXmonad.Shared.Theme.Color qualified as Color
import MyXmonad.Shared.Theme.Font qualified as Font
import System.FilePath qualified as FilePath
import Xmobar qualified


defaultXmobarConfig :: AppData -> Int -> Xmobar.Config
defaultXmobarConfig appData screenId =
  let
    position =
      Xmobar.OnScreen screenId (Xmobar.TopH (fromIntegral appData.theme.font.size + 10))
   in
    Xmobar.defaultConfig
      { Xmobar.font = Font.toPangoString $ appData.theme.font
      , Xmobar.textOutputFormat = Xmobar.Pango
      , Xmobar.position = position
      , Xmobar.borderColor = Color.toHashString appData.theme.xmobar.border
      , Xmobar.bgColor = Color.toHashString appData.theme.xmobar.background
      , Xmobar.fgColor = Color.toHashString appData.theme.xmobar.foreground
      , Xmobar.lowerOnStart = True
      , Xmobar.pickBroadest = False
      , Xmobar.persistent = False
      , Xmobar.hideOnStart = False
      , Xmobar.allDesktops = True
      , Xmobar.overrideRedirect = True
      }


primary :: AppData -> Xmobar.Config
primary appData =
  let
    paddingIconScript =
      FilePath.joinPath
        [ appData.filePaths.xdgConfig
        , "xmonad"
        , "xmobar"
        , "padding-icon.sh"
        ]
   in
    (defaultXmobarConfig appData 0)
      { Xmobar.commands =
          [ Xmobar.Run $ Xmobar.UnsafeXPropertyLog "_XMONAD_LOG_1"
          , Xmobar.Run $ Xmobar.Date "%a %b %_d %Y %I:%M:%S %p" "date" 10
          , Xmobar.Run $ Xmobar.ComX "mpc" ["current", "-f", "%title% by %artist%"] "" "mpd" 10
          , Xmobar.Run $ Xmobar.Com "bash" [paddingIconScript, "panel"] "tray" 10
          ]
      , Xmobar.template = "%_XMONAD_LOG_1% } %date% { %mpd% %tray%"
      }


secondary :: AppData -> Xmobar.Config
secondary appData =
  -- TODO Add weather, lookup location info from an environment variable
  (defaultXmobarConfig appData 2)
    { Xmobar.commands =
        [ Xmobar.Run $ Xmobar.UnsafeXPropertyLog "_XMONAD_LOG_2"
        ]
    , Xmobar.template = "%_XMONAD_LOG_2% } { "
    }


tertiary :: AppData -> Xmobar.Config
tertiary appData =
  (defaultXmobarConfig appData 1)
    { Xmobar.commands =
        [ Xmobar.Run $ Xmobar.UnsafeXPropertyLog "_XMONAD_LOG_3"
        , Xmobar.Run $ Xmobar.Memory ["-t", "Mem: <usedratio>%"] 10
        , Xmobar.Run $ Xmobar.Swap [] 10
        , Xmobar.Run $ Xmobar.Cpu [] 10
        ]
    , Xmobar.template = "%_XMONAD_LOG_3% } { %cpu% | %memory% * %swap%"
    }


start :: Config -> Args -> IO ()
start config args = do
  appData <- AppData.fromConfig config

  let
    xmobarConfig =
      case args of
        Primary -> primary
        Secondary -> secondary
        Tertiary -> tertiary

  Xmobar.xmobar $ xmobarConfig appData
