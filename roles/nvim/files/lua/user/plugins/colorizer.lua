return {
  "NvChad/nvim-colorizer.lua",
  config = function()
    local colorizer = require("colorizer")

    colorizer.setup({
      filetypes = {
        "html",
        "css",
      },
      user_default_options = {
        mode = "virtualtext",
      },
    })
  end,
  ft = { "html", "css" }
}
