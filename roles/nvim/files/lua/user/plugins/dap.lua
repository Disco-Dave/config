-- https://miguelcrespo.co/posts/debugging-javascript-applications-with-neovim

return {
  "rcarriga/nvim-dap-ui",
  dependencies = {
    "mfussenegger/nvim-dap",
    "nvim-neotest/nvim-nio",
    "mxsdev/nvim-dap-vscode-js",
    "julianolf/nvim-dap-lldb",
    "NicholasMata/nvim-dap-cs",
    {
      "microsoft/vscode-js-debug"
      ,
      build = "npm install --legacy-peer-deps && npx gulp vsDebugServerBundle && mv dist out"
    },
    {
      "Joakker/lua-json5",
      build = "./install.sh"
    }
  },
  config = function()
    local dap = require("dap")
    local dapui = require("dapui")
    local dap_ext_vscode = require("dap.ext.vscode")


    -- Set keymaps to control the debugger
    vim.keymap.set("n", "<F5>", require "dap".continue)
    vim.keymap.set("n", "<F10>", require "dap".step_over)
    vim.keymap.set("n", "<F11>", require "dap".step_into)
    vim.keymap.set("n", "<F12>", require "dap".step_out)
    vim.keymap.set("n", "<leader>b", require "dap".toggle_breakpoint)
    vim.keymap.set("n", "<leader>B", function()
      require "dap".set_breakpoint(vim.fn.input("Breakpoint condition: "))
    end)


    -- UI
    dapui.setup()
    dap.listeners.after.event_initialized["dapui_config"] = function()
      dapui.open({})
    end
    dap.listeners.before.event_terminated["dapui_config"] = function()
      dapui.close({})
    end
    dap.listeners.before.event_exited["dapui_config"] = function()
      dapui.close({})
    end

    vim.keymap.set("n", "<leader>ui", require "dapui".toggle)


    -- Javascript/Typescript
    require("dap-vscode-js").setup({
      debugger_path = vim.fn.stdpath("data") .. "/lazy/vscode-js-debug", -- Path to vscode-js-debug installation.
      adapters = {
        "chrome",
        "pwa-node",
        "pwa-chrome",
        "pwa-firefox",
        "node-terminal",
        "pwa-extensionHost",
        "node",
        "chrome",
        "firefox"
      }, -- which adapters to register in nvim-dap
    })

    local js_based_languages = { "typescript", "javascript", "typescriptreact" }
    for _, language in ipairs(js_based_languages) do
      dap.configurations[language] = {
        {
          type = "pwa-node",
          request = "launch",
          name = "Launch file",
          program = "${file}",
          cwd = "${workspaceFolder}",
        },
        {
          type = "pwa-node",
          request = "attach",
          name = "Attach",
          processId = require "dap.utils".pick_process,
          cwd = "${workspaceFolder}",
        },
        {
          type = "pwa-chrome",
          request = "launch",
          name = "Start Chrome with \"localhost\"",
          url = "http://localhost:3000",
          webRoot = "${workspaceFolder}",
          userDataDir = "${workspaceFolder}/.vscode/vscode-chrome-debug-userdatadir"
        },
        {
          type = "pwa-firefox",
          request = "launch",
          name = "Start Firefox with \"localhost\"",
          url = "http://localhost:3000",
          webRoot = "${workspaceFolder}",
          userDataDir = "${workspaceFolder}/.vscode/vscode-firefox-debug-userdatadir"
        }
      }
    end

    dap_ext_vscode.load_launchjs(nil,
      {
        ["pwa-node"] = js_based_languages,
        ["node"] = js_based_languages,
        ["chrome"] = js_based_languages,
        ["pwa-chrome"] = js_based_languages,
        ["firefox"] = js_based_languages,
        ["pwa-firefox"] = js_based_languages
      }
    )

    -- Rust
    require("dap-lldb").setup()

    -- .Net
    require("dap-cs").setup()
  end
}
