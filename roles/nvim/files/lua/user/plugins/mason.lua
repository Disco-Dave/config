return {
  "williamboman/mason.nvim",
  dependencies = {
    "williamboman/mason-lspconfig.nvim",
    "WhoIsSethDaniel/mason-tool-installer.nvim",
  },
  config = function()
    require("mason").setup()

    require("mason-tool-installer").setup({
      ensure_installed = {
        "codelldb",
        "netcoredbg",
        "sql-formatter",
      },
      auto_update = true,
    })

    require("mason-lspconfig").setup({
      -- https://github.com/williamboman/mason-lspconfig.nvim#available-lsp-servers
      ensure_installed = {
        "ansiblels",
        "csharp_ls",
        "cssls",
        "docker_compose_language_service",
        "dockerls",
        "efm",
        "eslint",
        "html",
        "jsonls",
        "lua_ls",
        "stylelint_lsp",
        "terraformls",
        "tflint",
        "ts_ls",
        "yamlls",
      }
    })
  end
}
