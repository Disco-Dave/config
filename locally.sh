#!/bin/bash

set -e

exec "$1" \
  --connection="local" \
  --limit="$(hostnamectl hostname)" \
  --ask-become-pass \
  "${@:2}"
