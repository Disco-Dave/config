module MyXmonad.Shared.Theme
  ( WindowColors (..)
  , XmobarColors (..)
  , DmenuColors (..)
  , Theme (..)
  , nord
  , vars
  )
where

import Env qualified
import MyXmonad.Shared.Theme.Color (Color)
import MyXmonad.Shared.Theme.Color qualified as Color
import MyXmonad.Shared.Theme.Font (Font)
import MyXmonad.Shared.Theme.Font qualified as Font
import MyXmonad.Shared.Theme.Nord qualified as Nord


data WindowColors = WindowColors
  { border :: Color
  , borderFocussed :: Color
  , borderUrgent :: Color
  , text :: Color
  , textFocussed :: Color
  , textUrgent :: Color
  }
  deriving (Show, Eq)


data XmobarColors = XmobarColors
  { border :: Color
  , background :: Color
  , foreground :: Color
  , selectedLayout :: Color
  , title :: Color
  , currentWs :: Color
  , urgentWs :: Color
  , windowTitle :: Color
  }
  deriving (Show, Eq)


data DmenuColors = DmenuColors
  { normalBackground :: Color
  , normalForeground :: Color
  , selectedBackground :: Color
  , selectedForeground :: Color
  }
  deriving (Show, Eq)


data Theme = Theme
  { font :: Font
  , window :: WindowColors
  , xmobar :: XmobarColors
  , dmenu :: DmenuColors
  }
  deriving (Show, Eq)


nord :: Theme
nord =
  Theme
    { font = Font.def
    , window =
        WindowColors
          { border = Nord.polarNight1
          , borderFocussed = Nord.frost2
          , borderUrgent = Nord.auroraRed
          , text = Nord.snowStorm2
          , textFocussed = Nord.polarNight1
          , textUrgent = Nord.snowStorm2
          }
    , xmobar =
        XmobarColors
          { border = Nord.polarNight1
          , background = Nord.polarNight1
          , foreground = Nord.snowStorm2
          , selectedLayout = Nord.auroraYellow
          , title = Nord.auroraGreen
          , currentWs = Nord.auroraYellow
          , windowTitle = Nord.auroraGreen
          , urgentWs = Nord.auroraRed
          }
    , dmenu =
        DmenuColors
          { normalBackground = Nord.polarNight1
          , normalForeground = Nord.snowStorm2
          , selectedBackground = Nord.frost2
          , selectedForeground = Nord.polarNight1
          }
    }


vars :: Theme -> Env.Parser Env.Error Theme
vars defVal =
  Env.prefixed "THEME_" $ do
    font <- Font.vars "" nord.font

    window <-
      Env.prefixed "WINDOW_" $ do
        border <- Color.var "BORDER_" defVal.window.border
        borderFocussed <- Color.var "BORDER_FOCUSSED_" defVal.window.borderFocussed
        borderUrgent <- Color.var "BORDER_URGENT_" defVal.window.borderUrgent
        text <- Color.var "TEXT_" defVal.window.text
        textFocussed <- Color.var "TEXT_FOCUSSED_" defVal.window.textFocussed
        textUrgent <- Color.var "TEXT_URGENT_" defVal.window.textUrgent
        pure WindowColors{..}

    xmobar <-
      Env.prefixed "XMOBAR_" $ do
        border <- Color.var "BORDER_" defVal.xmobar.border
        background <- Color.var "BACKGROUND_" defVal.xmobar.background
        foreground <- Color.var "FOREGROUND_" defVal.xmobar.foreground
        selectedLayout <- Color.var "SELECTED_LAYOUT_" defVal.xmobar.selectedLayout
        title <- Color.var "TITLE_" defVal.xmobar.title
        currentWs <- Color.var "CURRENT_WS_" defVal.xmobar.currentWs
        windowTitle <- Color.var "WINDOW_TITLE_" defVal.xmobar.windowTitle
        urgentWs <- Color.var "URGENT_WS_" defVal.xmobar.urgentWs
        pure XmobarColors{..}

    dmenu <-
      Env.prefixed "DMENU_" $ do
        normalBackground <- Color.var "NORMAL_BACKGROUND_" defVal.dmenu.normalBackground
        normalForeground <- Color.var "NORMAL_FOREGROUND_" defVal.dmenu.normalForeground
        selectedBackground <- Color.var "SELECTED_BACKGROUND_" defVal.dmenu.selectedBackground
        selectedForeground <- Color.var "SELECTED_FOREGROUND_" defVal.dmenu.selectedForeground
        pure DmenuColors{..}

    pure Theme{..}
