module MyXmobar.Args
  ( Args (..)
  , getArgs
  ) where

import System.Environment qualified as Environment


data Args
  = Primary
  | Secondary
  | Tertiary
  deriving (Show, Eq, Ord, Bounded, Enum)


getArgs :: IO Args
getArgs = do
  args <- Environment.getArgs

  pure $ case args of
    ("--secondary" : _) -> Secondary
    ("--tertiary" : _) -> Tertiary
    _other -> Primary
