module MyXmonad.Workspaces
  ( Workspace (..)
  , workspaces
  , workspaceNames
  , workspaceKeys
  )
where

import Graphics.X11 qualified as X11
import MyXmonad.Shared.AppData (AppData)
import MyXmonad.Shared.AppData qualified as AppData


data Workspace = Workspace
  { name :: String
  , key :: X11.KeySym
  }
  deriving (Show)


workspaces :: AppData -> [Workspace]
workspaces appData
  | length appData.screenInfo > 1 =
      let names = show @Int <$> ([2, 1] <> [3 ..])
          keys = [X11.xK_2, X11.xK_1] <> [X11.xK_3 .. X11.xK_9] <> [X11.xK_0]
       in zipWith Workspace names keys
  | otherwise =
      let names = show @Int <$> [1 ..]
          keys = [X11.xK_1 .. X11.xK_9] <> [X11.xK_0]
       in zipWith Workspace names keys


workspaceNames :: AppData -> [String]
workspaceNames =
  fmap (.name) . workspaces


workspaceKeys :: AppData -> [X11.KeySym]
workspaceKeys =
  fmap (.key) . workspaces
