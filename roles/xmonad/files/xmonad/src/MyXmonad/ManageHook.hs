module MyXmonad.ManageHook
  ( manageHook
  )
where

import XMonad ((-->), (=?))
import XMonad qualified
import XMonad.Hooks.ManageHelpers qualified as ManageHelpers


manageHook :: XMonad.ManageHook
manageHook =
  mconcat -- use xprop to find this information
    [ XMonad.className =? "Pavucontrol" --> ManageHelpers.doCenterFloat
    , XMonad.className =? "Gcolor3" --> ManageHelpers.doCenterFloat
    , XMonad.appName =? "xfce4-notifyd" --> XMonad.doIgnore
    ]
