module Main (main) where

import MyXmonad qualified
import MyXmonad.Shared.Config qualified as Config

main :: IO ()
main = do
  config <- Config.fromEnv
  MyXmonad.start config
