module MyXmonad.Shared.Theme.Font
  ( Font (..)
  , toPangoText
  , toPangoString
  , toXftText
  , toXftString
  , vars
  , def
  )
where

import Data.Text (Text)
import Data.Text qualified as Text
import Env qualified
import MyXmonad.Shared.Utils (showToText)
import Numeric.Natural (Natural)


data Font = Font
  { name :: Text
  , size :: Natural
  }
  deriving (Show, Eq)


toPangoText :: Font -> Text
toPangoText font =
  font.name <> " " <> showToText font.size


toPangoString :: Font -> String
toPangoString =
  Text.unpack . toPangoText


toXftText :: Font -> Text
toXftText font =
  "xft:" <> font.name <> ":size=" <> showToText font.size


toXftString :: Font -> String
toXftString =
  Text.unpack . toXftText


def :: Font
def =
  Font
    { name = "FreeSans"
    , size = 12
    }


vars :: String -> Font -> Env.Parser Env.Error Font
vars prefix defVal =
  Env.prefixed prefix . Env.prefixed "FONT_" $
    Font
      <$> Env.var Env.nonempty "NAME" (Env.help "Name of the font" <> Env.def defVal.name <> Env.helpDef Text.unpack)
      <*> Env.var Env.auto "SIZE" (Env.help "Size of the font." <> Env.def defVal.size <> Env.helpDef show)
