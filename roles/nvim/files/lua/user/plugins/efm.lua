return {
  "creativenull/efmls-configs-nvim",
  dependencies = {
    "neovim/nvim-lspconfig",
    "williamboman/mason.nvim",
  },
  config = function()
    -- Register linters and formatters per language
    -- local eslint = require("efmls-configs.linters.eslint_d")
    local ansible_lint = require("efmls-configs.linters.ansible_lint")
    local fourmolu = require("efmls-configs.formatters.fourmolu")
    local prettier = require("efmls-configs.formatters.prettier")
    local shellcheck = require("efmls-configs.linters.shellcheck")
    local shfmt = require("efmls-configs.formatters.shfmt")
    local sql_formatter = require("efmls-configs.formatters.sql-formatter")

    local languages = {
      ["yaml.ansible"] = { prettier, ansible_lint },
      bash = { shfmt, shellcheck },
      css = { prettier },
      haskell = { fourmolu },
      html = { prettier },
      javascript = { prettier },
      javascriptreact = { prettier },
      json = { prettier },
      sh = { shfmt, shellcheck },
      sql = { sql_formatter},
      typescript = { prettier },
      typescriptreact = { prettier },
      yaml = { prettier, ansible_lint },
      zsh = { shfmt },
    }

    -- Or use the defaults provided by this plugin
    -- check doc/SUPPORTED_LIST.md for the supported languages
    --
    -- local languages = require("efmls-configs.defaults").languages()

    local efmls_config = {
      filetypes = vim.tbl_keys(languages),
      settings = {
        rootMarkers = { ".git/" },
        languages = languages,
      },
      init_options = {
        documentFormatting = true,
        documentRangeFormatting = true,
      },
    }

    require("lspconfig").efm.setup(vim.tbl_extend("force", efmls_config, {
      -- Pass your custom lsp config below like on_attach and capabilities
      --
      -- on_attach = on_attach,
      -- capabilities = capabilities,
    }))
  end,
}
