module MyXmonad.Shared.Config
  ( Config (..)
  , vars
  , helpDoc
  , fromEnv
  ) where

import Env qualified
import GHC.Natural (Natural)
import MyXmonad.Shared.Theme (Theme)
import MyXmonad.Shared.Theme qualified as Theme


data Config = Config
  { theme :: Theme
  , startupSound :: Maybe FilePath
  , wallpaper :: Maybe FilePath
  , redshiftCoordinates :: Maybe String
  , dmenuFontSize :: Maybe Natural
  }
  deriving (Show, Eq)


startupSoundVar :: Env.Parser Env.Error (Maybe FilePath)
startupSoundVar =
  Env.optional . Env.var Env.str "STARTUP_SOUND" . mconcat $
    [ Env.help "Sound to play at startup"
    ]


wallpaperVar :: Env.Parser Env.Error (Maybe FilePath)
wallpaperVar =
  Env.optional . Env.var Env.str "WALLPAPER" . mconcat $
    [ Env.help "Image to set as the wallpaper"
    ]


redshiftCoordinatesVar :: Env.Parser Env.Error (Maybe FilePath)
redshiftCoordinatesVar =
  Env.optional . Env.var Env.str "REDSHIFT_COORDINATES" . mconcat $
    [ Env.help "Image to set as the wallpaper"
    ]


dmenuFontSizeVar :: Env.Parser Env.Error (Maybe Natural)
dmenuFontSizeVar =
  Env.optional . Env.var Env.auto "DMENU_FONT_SIZE" . mconcat $
    [ Env.help "Font size for dmenu"
    ]


vars :: Env.Parser Env.Error Config
vars = do
  let prefix = Env.prefixed "MY_XMONAD_"

  startupSound <- prefix startupSoundVar
  wallpaper <- prefix wallpaperVar
  theme <- prefix $ Theme.vars Theme.nord
  redshiftCoordinates <- redshiftCoordinatesVar
  dmenuFontSize <- prefix dmenuFontSizeVar

  pure Config{..}


helpDoc :: String
helpDoc =
  Env.helpDoc vars


fromEnv :: IO Config
fromEnv =
  Env.parse (Env.header "my-xmonad") vars
