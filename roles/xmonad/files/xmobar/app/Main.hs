module Main (main) where

import MyXmobar qualified
import MyXmobar.Args qualified as Args
import MyXmonad.Shared.Config qualified as Config


main :: IO ()
main = do
  args <- Args.getArgs
  config <- Config.fromEnv
  MyXmobar.start config args
