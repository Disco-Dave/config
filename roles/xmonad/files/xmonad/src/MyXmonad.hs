module MyXmonad (start) where

import MyXmonad.Hooks qualified as Hooks
import MyXmonad.Keys qualified as Keys
import MyXmonad.Layout qualified as LayoutHook
import MyXmonad.ManageHook qualified as ManageHook
import MyXmonad.Shared.AppData qualified as AppData
import MyXmonad.Shared.Config (Config)
import MyXmonad.Shared.Theme qualified as Theme
import MyXmonad.Shared.Theme.Color qualified as Color
import MyXmonad.StartupHook qualified as StartupHook
import MyXmonad.Workspaces (workspaceNames)
import XMonad qualified


start :: Config -> IO ()
start config = do
  appData <- AppData.fromConfig config

  let
    color windowColor =
      let theme = appData.theme
       in Color.toHashString $ windowColor theme.window

  XMonad.xmonad . Hooks.applyHooks appData $
    XMonad.def
      { XMonad.terminal = "alacritty"
      , XMonad.borderWidth = 3
      , XMonad.focusedBorderColor = color (.borderFocussed)
      , XMonad.normalBorderColor = color (.border)
      , XMonad.modMask = Keys.altKey
      , XMonad.keys = Keys.keys appData
      , XMonad.manageHook = ManageHook.manageHook
      , XMonad.startupHook = StartupHook.startupHook appData
      , XMonad.workspaces = workspaceNames appData
      , XMonad.layoutHook = LayoutHook.layoutHook
      }
