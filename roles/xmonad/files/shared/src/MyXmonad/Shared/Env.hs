-- | Generic functions for parsing values out of environment variables.
module MyXmonad.Shared.Env
  ( parseString
  , parseAuto
  , bool
  )
where

import Control.Monad.Reader (ReaderT (..))
import Control.Monad.Trans (MonadTrans (lift))
import Data.String (IsString)
import Data.Text qualified as Text
import Env qualified


-- | Parse a string link value out of an environment variable.
parseString :: (IsString str, Show str) => (str -> Maybe a) -> Env.Reader Env.Error a
parseString fromString = runReaderT $ do
  string <- ReaderT Env.nonempty
  lift $ case fromString string of
    Nothing -> Left . Env.UnreadError $ show string
    Just value -> pure value


-- | Parse an environment variable based off a 'Read' instance.
parseAuto :: (Read i, Show i) => (i -> Maybe a) -> Env.Reader Env.Error a
parseAuto fromInput = runReaderT $ do
  i <- ReaderT Env.auto
  lift $ case fromInput i of
    Nothing -> Left . Env.UnreadError $ show i
    Just value -> pure value


-- | Parse a bool value from an environment variable.
-- This function is purposefully lenient with what it accepts.
-- Some example values: true, false, yes, no, on, off, 1, 0, etc.
bool :: Env.Reader Env.Error Bool
bool =
  parseString $ \input ->
    let
      rawText =
        Text.toCaseFold $ Text.strip input

      possibleValues =
        [ (Text.toCaseFold "true", True)
        , (Text.toCaseFold "t", True)
        , (Text.toCaseFold "1", True)
        , (Text.toCaseFold "yes", True)
        , (Text.toCaseFold "y", True)
        , (Text.toCaseFold "on", True)
        , (Text.toCaseFold "false", False)
        , (Text.toCaseFold "f", False)
        , (Text.toCaseFold "0", False)
        , (Text.toCaseFold "no", False)
        , (Text.toCaseFold "n", False)
        , (Text.toCaseFold "off", False)
        ]
     in
      lookup rawText possibleValues
