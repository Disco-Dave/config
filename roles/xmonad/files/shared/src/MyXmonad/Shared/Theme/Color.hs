module MyXmonad.Shared.Theme.Color
  ( HexDigit (..)
  , Color (..)
  , digitToChar
  , charToDigit
  , fromText
  , toText
  , toString
  , toHashString
  , to0xString
  , var
  ) where

import Data.Char qualified as Char
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Env qualified
import MyXmonad.Shared.Env (parseString)


data HexDigit
  = HexDigit0
  | HexDigit1
  | HexDigit2
  | HexDigit3
  | HexDigit4
  | HexDigit5
  | HexDigit6
  | HexDigit7
  | HexDigit8
  | HexDigit9
  | HexDigitA
  | HexDigitB
  | HexDigitC
  | HexDigitD
  | HexDigitE
  | HexDigitF
  deriving (Show, Eq, Ord, Enum, Bounded)


digitToChar :: HexDigit -> Char
digitToChar = \case
  HexDigit0 -> '0'
  HexDigit1 -> '1'
  HexDigit2 -> '2'
  HexDigit3 -> '3'
  HexDigit4 -> '4'
  HexDigit5 -> '5'
  HexDigit6 -> '6'
  HexDigit7 -> '7'
  HexDigit8 -> '8'
  HexDigit9 -> '9'
  HexDigitA -> 'A'
  HexDigitB -> 'B'
  HexDigitC -> 'C'
  HexDigitD -> 'D'
  HexDigitE -> 'E'
  HexDigitF -> 'F'


charToDigit :: Char -> Maybe HexDigit
charToDigit char =
  case Char.toUpper char of
    '0' -> Just HexDigit0
    '1' -> Just HexDigit1
    '2' -> Just HexDigit2
    '3' -> Just HexDigit3
    '4' -> Just HexDigit4
    '5' -> Just HexDigit5
    '6' -> Just HexDigit6
    '7' -> Just HexDigit7
    '8' -> Just HexDigit8
    '9' -> Just HexDigit9
    'A' -> Just HexDigitA
    'B' -> Just HexDigitB
    'C' -> Just HexDigitC
    'D' -> Just HexDigitD
    'E' -> Just HexDigitE
    'F' -> Just HexDigitF
    _ -> Nothing


data Color = Color
  { red :: (HexDigit, HexDigit)
  , green :: (HexDigit, HexDigit)
  , blue :: (HexDigit, HexDigit)
  }
  deriving (Show, Eq, Ord)


toString :: Color -> String
toString color =
  let
    (r1, r2) = color.red
    (g1, g2) = color.green
    (b1, b2) = color.blue
   in
    fmap digitToChar [r1, r2, g1, g2, b1, b2]


toText :: Color -> Text
toText =
  Text.pack . toString


fromText :: Text -> Maybe Color
fromText text =
  let
    stripPrefix p t =
      fromMaybe t $ Text.stripPrefix p t

    strippedText =
      stripPrefix "#" . stripPrefix "0x" $ Text.strip text

    parsedDigits =
      traverse charToDigit $ Text.unpack strippedText
   in
    case parsedDigits of
      Just [r1, r2, g1, g2, b1, b2] ->
        Just
          Color
            { red = (r1, r2)
            , green = (g1, g2)
            , blue = (b1, b2)
            }
      _ ->
        Nothing


toHashString :: Color -> String
toHashString color =
  "#" <> toString color


to0xString :: Color -> String
to0xString color =
  "0x" <> toString color


var :: String -> Color -> Env.Parser Env.Error Color
var prefix defVal =
  Env.prefixed prefix . Env.prefixed "COLOR_" $
    Env.var (parseString fromText) "Color" . mconcat $
      [ Env.help "Hexadecimal number for a color."
      , Env.helpDef (Text.unpack . toText)
      , Env.def defVal
      ]
