local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  "christoomey/vim-tmux-navigator",
  "duff/vim-bufonly",
  "romainl/vim-cool",
  "sheerun/vim-polyglot",
  "tpope/vim-dadbod",
  "tpope/vim-fugitive",
  "vim-scripts/Tabmerge",
  require("user.plugins.autopairs"),
  require("user.plugins.cmp"),
  require("user.plugins.colorizer"),
  require("user.plugins.comment"),
  require("user.plugins.dap"),
  require("user.plugins.efm"),
  require("user.plugins.lspconfig"),
  require("user.plugins.lualine"),
  require("user.plugins.luatab"),
  require("user.plugins.mason"),
  require("user.plugins.nord"),
  require("user.plugins.nvim-tree"),
  require("user.plugins.rainbow"),
  require("user.plugins.slime"),
  require("user.plugins.treesitter"),
  require("user.plugins.vimwiki"),
})
