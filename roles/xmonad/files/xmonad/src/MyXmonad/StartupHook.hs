module MyXmonad.StartupHook
  ( startupHook
  )
where

import Data.Foldable (traverse_)
import Data.Maybe (catMaybes)
import MyXmonad.Shared.AppData (AppData)
import MyXmonad.Shared.AppData qualified as AppData
import MyXmonad.Shared.Theme qualified as Theme
import MyXmonad.Shared.Theme.Color qualified as Color
import MyXmonad.Shared.Theme.Font qualified as Font
import XMonad (X)
import XMonad qualified
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.StackSet qualified as StackSet
import XMonad.Util.SpawnOnce (spawnOnce)


focusPrimaryScreen :: X ()
focusPrimaryScreen = do
  primaryScreenId <- XMonad.screenWorkspace 0
  case primaryScreenId of
    Nothing -> pure ()
    Just screenId ->
      XMonad.windows $ StackSet.view screenId


startupHook :: AppData -> X ()
startupHook appData = do
  setWMName "LG3D" -- Fixes GUIs built with Java
  focusPrimaryScreen

  let
    trayer =
      "trayer "
        <> unwords
          [ "--monitor primary"
          , "--widthtype request"
          , "--SetDockType true"
          , "--SetPartialStrut true"
          , "--edge top"
          , "--align right"
          , "--alpha 0"
          , "--transparent true"
          , "--tint " <> Color.to0xString appData.theme.xmobar.background
          , "--iconspacing 3"
          , "--height " <> show (appData.theme.font.size + 10)
          ]

  traverse_ spawnOnce . catMaybes $
    [ Just "lxqt-policykit-agent"
    , fmap ("feh --no-fehbg --bg-center --no-xinerama " <>) appData.wallpaper
    , Just trayer
    , Just "picom"
    , Just "/usr/lib/xfce4/notifyd/xfce4-notifyd"
    , Just "xfce4-power-manager"
    , Just "pasystray"
    , Just "nm-applet"
    , Just "blueman-applet"
    , fmap ("mpv " <>) appData.startupSound
    , fmap ("redshift-gtk -l " <>) appData.redshiftCoordinates
    , Just "birdtray -H"
    ]
